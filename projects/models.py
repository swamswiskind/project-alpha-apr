from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from teams.models import Team

# Create your models here.


class ProjectCategory(models.Model):
    name = models.CharField(max_length=200)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="project_categories",
        null=True,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Project Category"
        verbose_name_plural = "Project Categories"


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    category = models.ForeignKey(
        ProjectCategory,
        related_name="projects",
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return self.name


def create_default_project_category(user, team):
    default_category = ProjectCategory(name="General", team=team, user=user)
    default_category.save()
