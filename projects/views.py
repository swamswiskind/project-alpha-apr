from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.db.models import Q
from projects.models import Project, ProjectCategory
from projects.forms import (
    ProjectForm,
    ProjectCategoryForm,
    UpdateProjectForm,
    ProjectModalForm,
)
from tasks.models import Task

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(
        Q(owner=request.user) | Q(tasks__assignee=request.user)
    ).distinct()
    tasks = Task.objects.filter(assignee=request.user)
    num_categories = ProjectCategory.objects.count()
    total_progress_count = 0
    total_remaining_count = 0

    for project in projects:
        remaining = Task.objects.filter(project=project, is_completed=False)
        progress = Task.objects.filter(project=project, is_completed=True)
        progress_count = progress.count()
        remaining_count = remaining.count()
        total_count = progress_count + remaining_count

        total_progress_count += progress_count
        total_remaining_count += remaining_count

        if total_count > 0:
            project.progress_percentage = int(
                progress_count / total_count * 100
            )
            project.remaining_percentage = int(
                100 - project.progress_percentage
            )
        else:
            project.progress_percentage = 0
            project.remaining_percentage = 100

    overall_total_count = total_progress_count + total_remaining_count
    if overall_total_count > 0:
        overall_progress_percentage = int(
            total_progress_count / overall_total_count * 100
        )
        overall_remaining_percentage = int(100 - overall_progress_percentage)
    else:
        overall_progress_percentage = 0
        overall_remaining_percentage = 100

    context = {
        "projects": projects,
        "entry_label": "projects",
        "tasks": tasks,
        "num_categories": num_categories,
        "overall_progress_percentage": overall_progress_percentage,
        "overall_remaining_percentage": overall_remaining_percentage,
        "project_form": ProjectForm(),
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    projects = Project.objects.filter(owner=request.user)
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project)
    total_remaining = Task.objects.filter(project=project, is_completed=False)
    total_progress = Task.objects.filter(project=project, is_completed=True)
    total_progress_count = total_progress.count()
    total_remaining_count = total_remaining.count()
    total_count = total_progress_count + total_remaining_count
    if total_count > 0:
        total_progress_percentage = int(
            total_progress_count / total_count * 100
        )
        total_remaining_percentage = int(100 - total_progress_percentage)
    else:
        total_progress_percentage = 20
        total_remaining_percentage = 100

    distinct_categories = project.tasks.values_list(
        "category__name", flat=True
    ).distinct()
    tasks_by_category = {}
    for category in distinct_categories:
        tasks = Task.objects.filter(project=project, category__name=category)
        tasks_by_category[category] = tasks
    tasks_without_category = Task.objects.filter(
        project=project, category__isnull=True
    )

    # Calculate progress and remaining counts and percentages for tasks without category
    tasks_without_category_count = tasks_without_category.count()
    tasks_without_category_progress_count = tasks_without_category.filter(
        is_completed=True
    ).count()
    tasks_without_category_remaining_count = tasks_without_category.filter(
        is_completed=False
    ).count()
    if tasks_without_category_count > 0:
        tasks_without_category_progress_percentage = int(
            tasks_without_category_progress_count
            / tasks_without_category_count
            * 100
        )
        tasks_without_category_remaining_percentage = (
            100 - tasks_without_category_progress_percentage
        )
    else:
        tasks_without_category_progress_percentage = 0
        tasks_without_category_remaining_percentage = 100

    context = {
        "project": project,
        "projects": projects,
        "tasks": tasks,
        "entry_label": "tasks",
        "total_count": total_count,
        "total_remaining": total_remaining,
        "total_remaining_count": total_remaining_count,
        "total_progress_count": total_progress_count,
        "total_progress_percentage": total_progress_percentage,
        "total_remaining_percentage": total_remaining_percentage,
        "distinct_categories": distinct_categories,
        "tasks_by_category": tasks_by_category,
        "tasks_without_category": tasks_without_category,
        "tasks_without_category_count": tasks_without_category_count,
        "tasks_without_category_progress_count": tasks_without_category_progress_count,
        "tasks_without_category_remaining_count": tasks_without_category_remaining_count,
        "tasks_without_category_progress_percentage": tasks_without_category_progress_percentage,
        "tasks_without_category_remaining_percentage": tasks_without_category_remaining_percentage,
    }

    return render(request, "projects/project_detail.html", context)


@login_required
def create_project_modal(request):
    team = request.user.teams.first()

    if request.method == "POST":
        form = ProjectModalForm(
            request.POST or None,
            user=request.user,
            team=request.user.teams.first(),
        )
        if form.is_valid():
            project = form.save
            return JsonResponse(
                {
                    "success": True,
                    "project": {
                        "id": project.id,
                        "name": project.name,
                        "description": project.description,
                        "categpry": project.category,
                        "owner": project.owner.username,
                        "team": team,
                    },
                }
            )
        else:
            return JsonResponse({"success": False, "errors": form.errors})
    else:
        form = ProjectModalForm(user=request.user, team=team)

    context = {"form": form, "team": team}
    return render(request, "projects/create_project_modal.html", context)


@login_required
def create_project(request):
    team = request.user.teams.first()

    if request.method == "POST":
        form = ProjectForm(
            request.POST or None,
            user=request.user,
            team=request.user.teams.first(),
        )
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = ProjectForm(user=request.user, team=team)

    context = {"form": form}

    return render(request, "projects/create_project.html", context)


@login_required
def update_project(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    if request.method == "POST":
        form = UpdateProjectForm(
            request.POST, instance=project, user=request.user
        )
        if form.is_valid():
            form.save()
            return redirect("show_project", project.id)
    else:
        form = UpdateProjectForm(instance=project, user=request.user)
    return render(request, "projects/update_project.html", {"form": form})


@login_required
def create_project_category(request):
    if request.method == "POST":
        form = ProjectCategoryForm(request.POST)
        if form.is_valid():
            project_category = form.save(
                commit=False
            )  # Save without committing the changes
            project_category.user = request.user  # Set the user
            project_category.save()  # Save the instance with the user and team set
            return redirect("list_projects")
    else:
        form = ProjectCategoryForm()
    return render(
        request, "projects/create_project_category.html", {"form": form}
    )
