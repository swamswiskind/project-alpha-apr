from django.contrib import admin
from projects.models import Project, ProjectCategory


# Register your models here.


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
        "id",
    )
    search_fields = ["name", "description"]
    list_filter = [
        ("owner", admin.RelatedOnlyFieldListFilter),
    ]


@admin.register(ProjectCategory)
class ProjectCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "team",
        "user",
        "id",
    )
    search_fields = [
        "name",
    ]
    list_filter = [
        ("team", admin.RelatedOnlyFieldListFilter),
        ("user", admin.RelatedOnlyFieldListFilter),
    ]
