from django.contrib.auth.models import User
from django import forms
from projects.models import Project, ProjectCategory
from teams.models import Team


class ProjectForm(forms.ModelForm):
    name = forms.CharField(
        label="Project name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    description = forms.CharField(
        label="Project Description",
        widget=forms.Textarea(attrs={"class": "form-control"}),
    )
    category = forms.ModelChoiceField(
        queryset=ProjectCategory.objects.all(),
        empty_label="Select a project category",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    owner = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label="Select an Project owner",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "category",
            "owner",
        )

    def __init__(self, *args, user=None, team=None, **kwargs):
        super().__init__(*args, **kwargs)
        if team:
            self.instance.team = team
        if user:
            self.instance.owner = user

            # Get the user's teams
            user_teams = user.teams.all()

            # Get unique categories associated with the user's teams
            self.fields["category"].queryset = ProjectCategory.objects.filter(
                team__in=user_teams
            ).distinct()

            # Get unique usernames associated with the user's teams
            self.fields["owner"].queryset = User.objects.filter(
                teams__in=user_teams
            ).distinct()


class ProjectModalForm(forms.ModelForm):
    name = forms.CharField(
        label="Project name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    description = forms.CharField(
        label="Project Description",
        widget=forms.Textarea(attrs={"class": "form-control"}),
    )
    category = forms.ModelChoiceField(
        queryset=ProjectCategory.objects.all(),
        empty_label="Select a project category",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    owner = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label="Select an Project owner",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "category",
            "owner",
        )

    def __init__(self, *args, user=None, team=None, **kwargs):
        super().__init__(*args, **kwargs)
        if team:
            self.instance.team = team
        if user:
            self.instance.owner = user

            # Get the user's teams
            user_teams = user.teams.all()

            # Get unique categories associated with the user's teams
            self.fields["category"].queryset = ProjectCategory.objects.filter(
                team__in=user_teams
            ).distinct()

            # Get unique usernames associated with the user's teams
            self.fields["owner"].queryset = User.objects.filter(
                teams__in=user_teams
            ).distinct()


class UpdateProjectForm(forms.ModelForm):
    name = forms.CharField(
        label="Project name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    description = forms.CharField(
        label="Description",
        widget=forms.Textarea(attrs={"class": "form-control"}),
    )
    owner = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label="Select an owner",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    category = forms.ModelChoiceField(
        queryset=ProjectCategory.objects.all(),
        empty_label="Select a category",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    class Meta:
        model = Project
        fields = ["name", "description", "owner", "category"]

    def __init__(self, *args, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        if user is not None:
            # Get the user's teams
            user_teams = user.teams.all()

            # Get unique categories associated with the user's teams
            self.fields["category"].queryset = ProjectCategory.objects.filter(
                team__in=user_teams
            ).distinct()

            # Get unique usernames associated with the user's teams
            self.fields["owner"].queryset = User.objects.filter(
                teams__in=user_teams
            ).distinct()


class ProjectCategoryForm(forms.ModelForm):
    name = forms.CharField(
        label="Category name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    team = forms.ModelChoiceField(
        queryset=Team.objects.all(),
        empty_label="Select a team",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    class Meta:
        model = ProjectCategory
        fields = ["name", "team"]
