from django.urls import path
from projects.views import (
    list_projects,
    show_project,
    create_project,
    create_project_category,
    update_project,
    create_project_modal,
)

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("create_modal/", create_project_modal, name="create_project_modal"),
    path(
        "create_project_category/",
        create_project_category,
        name="create_project_category",
    ),
    path(
        "update_project/<int:project_id>/",
        update_project,
        name="update_project",
    ),
]
