from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from projects.utils import create_default_project_category


class Command(BaseCommand):
    help = "Create default project categories for existing users"

    def handle(self, *args, **options):
        users = User.objects.all()
        for user in users:
            create_default_project_category(user)
            self.stdout.write(
                self.style.SUCCESS(
                    f"Default project category created for user: {user.username}"
                )
            )
