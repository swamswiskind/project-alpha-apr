# projects/utils.py

from projects.models import ProjectCategory


def create_default_project_category(user):
    general_category, created = ProjectCategory.objects.get_or_create(
        name="General",
        user=user,  # Add the user here
    )
    return general_category
