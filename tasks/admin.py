from django.contrib import admin
from tasks.models import Task, TaskStatus, TaskCategory

# Register your models here.


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "assignee",
        "status",
        "category",
        "project",
        "is_completed",
        "id",
    )
    search_fields = ["name"]
    list_filter = [
        ("is_completed", admin.BooleanFieldListFilter),
        ("assignee", admin.RelatedOnlyFieldListFilter),
        ("project", admin.RelatedOnlyFieldListFilter),
        ("start_date", admin.DateFieldListFilter),
        ("due_date", admin.DateFieldListFilter),
    ]


@admin.register(TaskStatus)
class TaskStatusAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "category",
        "user",
        "team",
    )
    search_fields = ["name"]
    list_filter = [
        ("user", admin.RelatedOnlyFieldListFilter),
        ("team", admin.RelatedOnlyFieldListFilter),
    ]


@admin.register(TaskCategory)
class TaskCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "project",
        "team",
    )
    search_fields = ["name"]
    list_filter = [
        ("user", admin.RelatedOnlyFieldListFilter),
        ("team", admin.RelatedOnlyFieldListFilter),
    ]
