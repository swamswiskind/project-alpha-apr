from django.db import models
from projects.models import Project
from teams.models import Team
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User


# Create your models here.


class TaskStatus(models.Model):
    STATUS_CATEGORIES = [
        ("backlog", "Backlog"),
        ("in_progress", "In Progress"),
        ("completed", "Completed"),
        ("closed", "Closed"),
        ("blocked", "Blocked"),
    ]
    name = models.CharField(max_length=200)
    category = models.CharField(max_length=20, choices=STATUS_CATEGORIES)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="task_statuses",
        null=True,
        on_delete=models.CASCADE,
    )
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Task Status"
        verbose_name_plural = "Task Statuses"
        unique_together = (
            "name",
            "team",
        )


class TaskCategory(models.Model):
    name = models.CharField(max_length=255)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, null=True, blank=True
    )
    team = models.ForeignKey(
        Team, on_delete=models.CASCADE, null=True, blank=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="task_categories",
        null=True,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Task Category"
        verbose_name_plural = "Task Categories"
        unique_together = (
            "name",
            "project",
            "team",
        )


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        null=True,
        on_delete=models.CASCADE,
    )
    status = models.ForeignKey(
        TaskStatus,
        related_name="tasks",
        null=True,
        on_delete=models.SET_NULL,
    )
    is_completed = models.BooleanField(default=False)
    category = models.ForeignKey(
        TaskCategory, on_delete=models.SET_NULL, null=True, blank=True
    )

    def save(self, *args, **kwargs):
        if self.status:
            self.is_completed = self.status.category in ["completed", "closed"]
        else:
            self.is_completed = False
        super(Task, self).save(*args, **kwargs)


# @receiver(post_save, sender=User)
# def create_default_task_status(sender, instance, created, **kwargs):
#     if created:
#         default_task_statuses = [
#             {"name": "To Do", "category": "backlog"},
#             {"name": "In Progress", "category": "in_progress"},
#             {"name": "Completed", "category": "completed"},
#             {"name": "Closed", "category": "closed"},
#             {"name": "Blocked", "category": "blocked"},
#         ]

#         for task_status in default_task_statuses:
#             TaskStatus.objects.create(
#                 name=task_status["name"],
#                 category=task_status["category"],
#                 user=instance,
#             )
