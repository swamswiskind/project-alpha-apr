from django.shortcuts import (
    render,
    redirect,
    get_object_or_404,
    HttpResponseRedirect,
)
from django.urls import reverse
from tasks.models import Task, Project
from tasks.forms import (
    TaskForm,
    TaskStatusForm,
    UpdateTaskStatusForm,
    UpdateTaskForm,
)
from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required
def create_task(request):
    team = request.user.teams.first()

    if request.method == "POST":
        form = TaskForm(request.POST, user=request.user, team=team)
        if form.is_valid():
            task = form.save(commit=False)
            task.save()
            # Redirect to the project detail page for the project this task belongs to
            return HttpResponseRedirect(
                reverse("show_project", args=[task.project.id])
            )
    else:
        form = TaskForm(user=request.user, team=team)
    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def update_task(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    team = request.user.teams.first()

    if request.method == "POST":
        form = UpdateTaskForm(
            request.POST,
            instance=task,
            user=request.user,
            team=team,
        )
        if form.is_valid():
            form.save()
            # Get the 'next' value from the POST data
            next_url = request.POST.get("next", "/")

            # Redirect to the referrer URL or a default URL if the referrer is not available
            return HttpResponseRedirect(next_url)
    else:
        form = UpdateTaskForm(instance=task, user=request.user, team=team)
    return render(
        request, "tasks/update_task.html", {"form": form, "task": task}
    )


@login_required
def update_task_status(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if request.method == "POST":
        form = UpdateTaskStatusForm(
            request.POST, instance=task, user=request.user
        )
        if form.is_valid():
            form.save()
            next_url = request.POST.get("next", "/")

            # Redirect to the referrer URL or a default URL if the referrer is not available
            return HttpResponseRedirect(next_url)
    else:
        form = UpdateTaskStatusForm(instance=task, user=request.user)
    return render(
        request, "tasks/update_task_status.html", {"form": form, "task": task}
    )


@login_required
def create_task_status(request):
    if request.method == "POST":
        form = TaskStatusForm(request.POST)
        if form.is_valid():
            task_status = form.save(commit=False)
            task_status.user = request.user
            task_status.save()
            # Redirect to the desired page after creating a new task status
            return redirect("list_projects")
    else:
        form = TaskStatusForm()
    return render(request, "tasks/create_task_status.html", {"form": form})


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    projects = Project.objects.filter(owner=request.user)
    remaining = Task.objects.filter(assignee=request.user, is_completed=False)
    progress = Task.objects.filter(assignee=request.user, is_completed=True)
    progress_count = progress.count()
    remaining_count = remaining.count()
    total_count = progress_count + remaining_count
    if total_count > 0:
        progress_percentage = int(progress_count / total_count * 100)
        remaining_percentage = int(100 - progress_percentage)
        progress_rounded = round(progress_percentage)
        remaining_rounded = round(remaining_percentage)

    else:
        progress_percentage = 0
        remaining_percentage = 100
    task = Task.objects.filter(assignee=request.user)
    context = {
        "projects": projects,
        "task": task,
        "tasks": tasks,
        "entry_label": "tasks",
        "remaining_count": remaining_count,
        "progress_count": progress_count,
        "progress_percentage": progress_percentage,
        "remaining_percentage": remaining_percentage,
        "progress_rounded": progress_rounded,
        "remaining_rounded": remaining_rounded,
    }

    return render(request, "tasks/list_tasks.html", context)
