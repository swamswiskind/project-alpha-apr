from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from tasks.models import TaskStatus


class Command(BaseCommand):
    help = "Create default Task Status Categories for all existing users"

    def handle(self, *args, **options):
        default_task_statuses = [
            {"name": "To Do", "category": "backlog"},
            {"name": "In Progress", "category": "in_progress"},
            {"name": "Completed", "category": "completed"},
            {"name": "Closed", "category": "closed"},
            {"name": "Blocked", "category": "blocked"},
        ]

        users = User.objects.all()

        for user in users:
            for task_status in default_task_statuses:
                TaskStatus.objects.get_or_create(
                    name=task_status["name"],
                    category=task_status["category"],
                    user=user,
                )

        self.stdout.write(
            self.style.SUCCESS(
                "Default Task Status Categories created for all existing users"
            )
        )
