from django.urls import path
from tasks.views import (
    create_task,
    show_my_tasks,
    update_task,
    create_task_status,
    update_task_status,
)

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("update_task/<int:task_id>/", update_task, name="update_task"),
    path("create_task_status/", create_task_status, name="create_task_status"),
    path(
        "update_task_status/<int:task_id>/",
        update_task_status,
        name="update_task_status",
    ),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
