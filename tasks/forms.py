from django import forms
from django.db.models import Q
from django.db.models import Case, When, Value, IntegerField
from django.contrib.auth.models import User
from tasks.models import Task, TaskStatus, TaskCategory
from projects.models import Project


class TaskForm(forms.ModelForm):
    name = forms.CharField(
        label="Project name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    status = forms.ModelChoiceField(
        queryset=TaskStatus.objects.all(),
        empty_label="Select a starting status",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    project = forms.ModelChoiceField(
        queryset=Project.objects.all(),
        empty_label="Select a project for this task",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    category = forms.ModelChoiceField(
        queryset=TaskCategory.objects.all(),
        empty_label="Select a task category",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    assignee = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label="Select an assignee",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "status",
            "project",
            "category",
            "assignee",
        ]

    def __init__(self, *args, user=None, team=None, project=None, **kwargs):
        super().__init__(*args, **kwargs)
        if user and team:
            user_projects = Project.objects.filter(
                Q(owner=user) | Q(tasks__assignee=user)
            ).distinct()

            self.fields["project"].queryset = user_projects
            self.fields["assignee"].queryset = User.objects.filter(teams=team)

            completed_statuses = ["completed", "closed"]
            status_order = Case(
                *[
                    When(name=x[0], then=Value(i))
                    for i, x in enumerate(TaskStatus.STATUS_CATEGORIES)
                    if x[0] in completed_statuses
                ],
                *[
                    When(name=x[0], then=Value(i + len(completed_statuses)))
                    for i, x in enumerate(
                        sorted(
                            TaskStatus.STATUS_CATEGORIES, key=lambda x: x[1]
                        )
                    )
                    if x[0] not in completed_statuses
                ],
                output_field=IntegerField()
            )
            self.fields["status"].queryset = (
                TaskStatus.objects.filter(team=team)
                .annotate(status_order=status_order)
                .order_by("status_order")
                .distinct()
            )
            if project:
                self.fields["category"].queryset = TaskCategory.objects.filter(
                    Q(project=project) | Q(team=project.team)
                ).distinct()


class UpdateTaskForm(TaskForm):
    name = forms.CharField(
        label="Project name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    status = forms.ModelChoiceField(
        queryset=TaskStatus.objects.all(),
        empty_label="Select a starting status",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    project = forms.ModelChoiceField(
        queryset=Project.objects.all(),
        empty_label="Select a project for this task",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    category = forms.ModelChoiceField(
        queryset=TaskCategory.objects.all(),
        empty_label="Select a task category",
        widget=forms.Select(attrs={"class": "form-select"}),
    )
    assignee = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label="Select an assignee",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "status",
            "project",
            "category",
            "assignee",
        ]

    def __init__(self, *args, user=None, team=None, **kwargs):
        super().__init__(*args, **kwargs)
        if user and team:
            user_projects = Project.objects.filter(
                Q(owner=user) | Q(tasks__assignee=user)
            ).distinct()

            self.fields["project"].queryset = user_projects
            self.fields["assignee"].queryset = User.objects.filter(teams=team)

            completed_statuses = ["completed", "closed"]
            status_order = Case(
                *[
                    When(name=x[0], then=Value(i))
                    for i, x in enumerate(TaskStatus.STATUS_CATEGORIES)
                    if x[0] in completed_statuses
                ],
                *[
                    When(name=x[0], then=Value(i + len(completed_statuses)))
                    for i, x in enumerate(
                        sorted(
                            TaskStatus.STATUS_CATEGORIES, key=lambda x: x[1]
                        )
                    )
                    if x[0] not in completed_statuses
                ],
                output_field=IntegerField()
            )
            self.fields["status"].queryset = (
                TaskStatus.objects.filter(team=team)
                .annotate(status_order=status_order)
                .order_by("status_order")
                .distinct()
            )


class TaskCategoryForm(forms.ModelForm):
    class Meta:
        model = TaskCategory
        fields = ("name",)

    def __init__(self, *args, team=None, project=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update({"class": "form-control"})

        if team:
            self.instance.team = team
        if project:
            self.instance.project = project


class TaskStatusForm(forms.ModelForm):
    class Meta:
        model = TaskStatus
        fields = ["name", "category"]


class UpdateTaskStatusForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ["status"]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

        self.fields["status"] = forms.ModelChoiceField(
            queryset=TaskStatus.objects.filter(user=user)
            .annotate(
                custom_order=Case(
                    When(category="completed", then=Value(1)),
                    When(category="closed", then=Value(1)),
                    default=Value(0),
                    output_field=IntegerField(),
                )
            )
            .order_by("custom_order", "name"),
            empty_label="Select a status",
            widget=forms.Select(attrs={"class": "form-select"}),
        )
