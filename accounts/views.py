from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignUpForm, TeamNameForm
from teams.models import Team
from accounts.models import TeamInvitation
from tasks.models import TaskStatus
from projects.models import create_default_project_category


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request, invitation_token=None):
    print("Signup view called")  # Add this line
    team = None

    def create_default_task_statuses(team):
        default_statuses = [
            {"name": "To Do", "category": "backlog"},
            {"name": "In Progress", "category": "in_progress"},
            {"name": "Completed", "category": "completed"},
            {"name": "Closed", "category": "closed"},
            {"name": "Blocked", "category": "blocked"},
        ]
        for status in default_statuses:
            TaskStatus.objects.create(
                name=status["name"],
                category=status["category"],
                team=team,
            )

    if request.method == "POST":
        print("POST request")  # Add this line
        form = SignUpForm(request.POST)
        print(form.errors)  # Add this line

        if form.is_valid():
            print("Form is valid")  # Add this line
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            team_name = form.cleaned_data["team_name"]

            if password == password_confirmation:
                print("Passwords match")  # Add this line

                # Check if a user with the provided username already exists
                if User.objects.filter(username=username).exists():
                    form.add_error(
                        "username", "A user with this username already exists."
                    )
                else:
                    print("Creating user")  # Add this line
                    user = User.objects.create_user(
                        username=username,
                        password=password,
                    )
                    login(request, user)
                    if not invitation_token and not team_name:
                        form.add_error("team_name", "This field is required.")

                    if invitation_token:
                        print(
                            "Invitation token:", invitation_token
                        )  # Add this line

                        try:
                            invitation = TeamInvitation.objects.get(
                                token=invitation_token
                            )
                            team = invitation.team

                            print(
                                "Invitation found:", invitation
                            )  # Add this line
                            team = invitation.team
                            print("Team is: ", team)
                            team.members.add(user)
                            # create_default_task_statuses(team)
                            invitation.delete()
                        except TeamInvitation.DoesNotExist:
                            print("Invitation not found")  # Add this line
                            pass
                    else:
                        team = Team.objects.create(name=team_name, owner=user)
                        team.members.add(user)
                        create_default_task_statuses(team)

                    # Call create_default_project_category with both user and team
                    create_default_project_category(user=user, team=team)

                    return redirect("list_projects")

            else:
                form.add_error("password", "the passwords do not match.")
    else:
        form = SignUpForm()

        if invitation_token:
            try:
                invitation = TeamInvitation.objects.get(token=invitation_token)
                team = invitation.team
            except TeamInvitation.DoesNotExist:
                pass

    context = {
        "form": form,
        "invitation_token": invitation_token,
        "team": team,
    }
    return render(request, "accounts/signup.html", context)


# def signup(request, invitation_token=None):
#     if request.method == "POST":
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             username = form.cleaned_data["username"]
#             password = form.cleaned_data["password"]
#             password_confirmation = form.cleaned_data["password_confirmation"]
#             team_name = form.cleaned_data["team_name"]

#             if password == password_confirmation:
#                 # Check if a user with the provided username already exists
#                 if User.objects.filter(username=username).exists():
#                     form.add_error(
#                         "username", "A user with this username already exists."
#                     )
#                 else:
#                     user = User.objects.create_user(
#                         username=username,
#                         password=password,
#                     )
#                     login(request, user)

#                     if invitation_token:
#                         try:
#                             invitation = TeamInvitation.objects.get(
#                                 token=invitation_token
#                             )
#                             team = invitation.team
#                             team.members.add(user)
#                             invitation.delete()
#                         except TeamInvitation.DoesNotExist:
#                             pass
#                     else:
#                         team = Team.objects.create(name=team_name, owner=user)
#                         team.members.add(user)

#                     # Call create_default_project_category with both user and team
#                     create_default_project_category(user=user, team=team)

#                     return redirect("list_projects")

#             else:
#                 form.add_error("password", "the passwords do not match.")
#     else:
#         form = SignUpForm()

#     context = {
#         "form": form,
#         "invitation_token": invitation_token,
#     }
#     return render(request, "accounts/signup.html", context)
