from django.urls import path, re_path
from accounts.views import user_login, user_logout, signup

urlpatterns = [
    path("signup/", signup, name="signup"),
    re_path(
        r"^signup/(?P<invitation_token>[0-9a-f-]+)/$",
        signup,
        name="signup_with_invitation",
    ),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
]
