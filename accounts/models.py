from django.db import models
from django.conf import settings
from teams.models import Team
import uuid


class TeamInvitation(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    invited_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    token = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    def __str__(self):
        return f"{self.invited_by} invited to {self.team}"

    class Meta:
        verbose_name = "Team Invite"
        verbose_name_plural = "Team Invites"
