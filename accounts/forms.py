from django import forms
from django.core.exceptions import ValidationError
from teams.models import Team


def password_validator(value):
    import re

    if len(value) < 8 or len(value) > 12:
        raise ValidationError(
            "Password must be between 8 and 12 characters long."
        )
    if not re.search(r"\d", value):
        raise ValidationError("Password must contain at least one number.")
    if not re.search(r"[A-Za-z]", value):
        raise ValidationError("Password must contain at least one letter.")
    if not re.search(r'[!@#$%^&*(),.?":{}|<>]', value):
        raise ValidationError(
            "Password must contain at least one special character."
        )


class TeamNameForm(forms.ModelForm):
    name = forms.CharField(
        label="Team name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

    class Meta:
        model = Team
        fields = ["name"]


class SignUpForm(forms.Form):
    username = forms.CharField(
        max_length=150,
        help_text="Enter your username",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    password = forms.CharField(
        max_length=150,
        help_text="Password must be 8-12 characters long, include at least one number, one letter, and a special character (e.g., !@#$%^&*).",
        widget=forms.PasswordInput(attrs={"class": "form-control"}),
        validators=[password_validator],
    )
    password_confirmation = forms.CharField(
        max_length=150,
        help_text="Enter your password again",
        widget=forms.PasswordInput(attrs={"class": "form-control"}),
        validators=[password_validator],
    )
    team_name = forms.CharField(
        max_length=200,
        required=False,
        help_text="Enter a team name. You'll be able to add other users to your team in settings.",
        label="Team name",
        widget=forms.TextInput(
            attrs={
                "placeholder": "ex: Rescue Rangers",
                "class": "form-control",
            }
        ),
    )


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=150,
        help_text="Enter your username",
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "title": "Enter your username",
                "required": True,
            }
        ),
    )
    password = forms.CharField(
        max_length=150,
        help_text="Enter your password",
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control",
                "title": "Enter your password",
                "required": True,
            }
        ),
    )
