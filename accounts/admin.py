from django.contrib import admin
from accounts.models import TeamInvitation


@admin.register(TeamInvitation)
class TeamInvitationAdmin(admin.ModelAdmin):
    list_display = (
        "team",
        "invited_by",
        "token",
    )
    search_fields = ["team"]
    list_filter = [
        ("invited_by", admin.RelatedOnlyFieldListFilter),
    ]
    fields = ("team", "invited_by", "token")
    readonly_fields = ("token",)
