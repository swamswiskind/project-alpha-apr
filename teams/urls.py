from django.urls import path
from teams.views import (
    create_team,
    list_teams,
    reassign_team_owner,
    team_invitation,
    create_team_invitation,
    list_invitations,
    team_members,
    remove_member,
)

urlpatterns = [
    path("create/", create_team, name="create_team"),
    path("list/", list_teams, name="list_teams"),
    path(
        "reassign-team-owner/<int:team_id>/",
        reassign_team_owner,
        name="reassign_team_owner",
    ),
    path(
        "create_invitation/<int:team_id>/",
        team_invitation,
        name="team_invitation",
    ),
    path(
        "create_invitation/",
        create_team_invitation,
        name="create_team_invitation",
    ),
    path("list-invitations/", list_invitations, name="list_invitations"),
    path("<int:team_id>/members/", team_members, name="team_members"),
    path(
        "<int:team_id>/members/remove/<int:user_id>/",
        remove_member,
        name="remove_member",
    ),
]
