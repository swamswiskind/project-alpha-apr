from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from teams.forms import (
    TeamForm,
    CreateTeamInvitationForm,
    ReassignTeamOwnerForm,
)
from teams.models import Team
from accounts.models import TeamInvitation
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from django.urls import reverse


@login_required
def create_team(request):
    if request.method == "POST":
        form = TeamForm(request.POST)
        if form.is_valid():
            team = form.save(commit=False)
            team.owner = request.user
            team.save()
            team.members.add(request.user)
            team.save()
            return redirect(
                "list_teams"
            )  # Change this to the appropriate view
    else:
        form = TeamForm()
    return render(request, "teams/create_team.html", {"form": form})


@login_required
def team_invitation(request, team_id):
    team = get_object_or_404(Team, id=team_id, owner=request.user)
    invitation = TeamInvitation.objects.create(
        team=team, invited_by=request.user
    )
    invitation_link = request.build_absolute_uri(
        reverse(
            "signup_with_invitation",
            kwargs={"invitation_token": str(invitation.token)},
        )
    )

    context = {
        "team": team,
        "invitation_link": invitation_link,
    }

    return render(request, "teams/invitation.html", context)


@login_required
def create_team_invitation(request):
    if request.method == "POST":
        form = CreateTeamInvitationForm(request.POST, user=request.user)
        if form.is_valid():
            team = form.cleaned_data["team"]
            TeamInvitation.objects.create(team=team, invited_by=request.user)
            return redirect("list_invitations")
    else:
        form = CreateTeamInvitationForm(user=request.user)

    context = {"form": form}
    return render(request, "teams/create_team_invitation.html", context)


@login_required
def list_teams(request):
    teams = request.user.teams.all()
    context = {"teams": teams}
    return render(request, "teams/list_teams.html", context)


@login_required
def list_invitations(request):
    if request.user.is_authenticated:
        invitations = TeamInvitation.objects.filter(team__owner=request.user)
        return render(
            request,
            "teams/list_invitations.html",
            {"invitations": invitations},
        )
    else:
        # Redirect to login page or show an error message
        pass


@login_required
def reassign_team_owner(request, team_id):
    team = get_object_or_404(Team, id=team_id, owner=request.user)

    if request.method == "POST":
        form = ReassignTeamOwnerForm(request.POST, team=team)
        if form.is_valid():
            new_owner = form.cleaned_data["new_owner"]
            team.owner = new_owner
            team.save()
            return redirect("list_teams")
    else:
        form = ReassignTeamOwnerForm(team=team)

    context = {"form": form, "team": team}
    return render(request, "teams/reassign_team_owner.html", context)


@login_required
def team_members(request, team_id):
    team = get_object_or_404(Team, id=team_id, owner=request.user)
    members = team.members.all()
    return render(
        request, "teams/team_members.html", {"teams": team, "members": members}
    )


def remove_member(request, team_id, user_id):
    team = get_object_or_404(Team, id=team_id, owner=request.user)
    user_to_remove = get_object_or_404(User, id=user_id)

    if request.method == "POST":
        reassign_option = request.POST.get("reassign_option")

        if reassign_option == "single":
            new_assignee_id = request.POST.get("new_assignee")
            new_assignee = get_object_or_404(User, id=new_assignee_id)

            tasks_to_reassign = Task.objects.filter(
                assignee=user_to_remove, project__team=team
            )
            print(tasks)
            tasks_to_reassign.update(assignee=new_assignee)

        elif reassign_option == "individual":
            task_ids = request.POST.getlist("task_id")
            for task_id in task_ids:
                task = get_object_or_404(Task, id=task_id)
                new_assignee_id = request.POST.get(f"new_assignee_{task_id}")
                new_assignee = get_object_or_404(User, id=new_assignee_id)
                task.assignee = new_assignee
                task.save()

        team.members.remove(user_to_remove)
        messages.success(
            request,
            f"{user_to_remove.username} has been removed from the team and their tasks have been reassigned.",
        )

        other_teams = user_to_remove.team_set.exclude(id=team.id)
        if not other_teams.exists():
            new_team = Team(
                name=f"{user_to_remove.username}'s Team", owner=user_to_remove
            )
            new_team.save()
            new_team.members.add(user_to_remove)
            messages.success(
                request,
                f"A new team has been created for {user_to_remove.username}",
            )

        return redirect("teams:team_members", team_id=team.id)

    other_members = team.members.exclude(id=user_to_remove.id)
    return render(
        request,
        "teams/remove_member_confirm.html",
        {
            "team": team,
            "user_to_remove": user_to_remove,
            "other_members": other_members,
        },
    )
