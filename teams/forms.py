from django import forms
from django.contrib.auth.models import User
from teams.models import Team
from accounts.models import TeamInvitation


class TeamForm(forms.ModelForm):
    name = forms.CharField(
        label="Team name",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

    class Meta:
        model = Team
        fields = ["name"]


class CreateTeamInvitationForm(forms.Form):
    team = forms.ModelChoiceField(
        queryset=Team.objects.all(),
        empty_label="Select a team",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    class Meta:
        model = TeamInvitation
        fields = ["team"]

    def __init__(self, *args, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        if user is not None:
            self.fields["team"].queryset = Team.objects.filter(owner=user)


class ReassignTeamOwnerForm(forms.Form):
    new_owner = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label="Select a new owner",
        widget=forms.Select(attrs={"class": "form-select"}),
    )

    def __init__(self, *args, team=None, **kwargs):
        super().__init__(*args, **kwargs)
        if team is not None:
            self.fields["new_owner"].queryset = team.members.all()
